package cucumberATDD;

import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;
import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;

import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumber-reports/cucumber.json",
		detailedReport = true,
		detailedAggregatedReport = true,
		overviewReport = true,
		jsonUsageReport = "target/cucumber-usage.json",
		usageReport = false,
		toPDF = true,
		outputFolder = "target/cucumber-pdf-reports/")
@CucumberOptions(
		plugin = {"pretty",
				"html:target/cucumber-reports/cucumber.html",
				"json:target/cucumber-reports/cucumber.json",
				"junit:target/cucumber-reports/cucumber-results.xml"
		},
		features = "classpath:cucumberATDD",
		glue = "cucumberATDD"
		, tags = {"@all"}
)
public class RunCucumberTest {
}
