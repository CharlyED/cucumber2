package cucumberATDD;

public class IsItFriday {
	static String isItFriday(String today) {
   	 return "Friday".equals(today) ? "TGIF" : "Nope";
   }
	
	static String isItMonday(String today) {
		//Just a comment
		return "Monday".equals(today) ? "TGIF" : "Nope";
	}
}
